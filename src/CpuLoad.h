#include <arduino.h>

// updateCpuTime отвечает за обновление статистики загрузки Cpu
// updateCpuTime(false) - размещается в начале loop()
// updateCpuTime(true) - размещается в конце loop()
// printCpuTime выводит среднее время работы 10 циклов loop в мкс
// printCpuLoad выводит прикидочную нагрузку loop цикла

static long int cpuTime1 = 0;
static long int cpuTime2 = 0;
static int cpuLoadAvgTime = 0;
static long int cpuTimeLastUpdate = 0;

static void updateCpuTime(bool state) {
    switch(state) {
        case 0: cpuTime1 = micros();
            return;
        case 1: cpuTime2 = micros();
        cpuLoadAvgTime = cpuLoadAvgTime*0.9 + (cpuTime2 - cpuTime1);
            return;
    }
}

static void printCpuTime () {
    if (millis() - cpuTimeLastUpdate > 2000) {
        Serial.printf("Time for update: %ld \n", cpuLoadAvgTime);
        cpuTimeLastUpdate = millis();
    }
}

static void printCpuLoad () {
    if (millis() - cpuTimeLastUpdate > 2000) {
        Serial.printf("CPU Load: %ld \n", cpuLoadAvgTime/10000);
        cpuTimeLastUpdate = millis();
    }
}
