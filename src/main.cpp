#include <Arduino.h>
#include <ESP8266WiFi.h>
//#include <WiFiClient.h>
#include <ESP8266WebServer.h>
//#include <ESP8266mDNS.h>
#include <html.h>
#include <Graph.h>
#include <Sensor.h>
#include <WebSocketsServer.h>
#include <ArduinoJson.h>
#include "CpuLoad.h"

#include "arduinoFFT.h"
bool fftMode = true;
arduinoFFT FFT = arduinoFFT();


const char *ssid = "LabWork_1";

ESP8266WebServer server(80);
Sensor accelerometer = Sensor();
Graph graphAcc = Graph(256, 50);

double vReal[256];
double vImag[256];

long int lastDataSend = 0;

int timeOfCal = 0;
bool accCalibrated = false;
int accCalRemains = 0;
int accMax1 = 0;
int accMax2 = 0;

WebSocketsServer webSocket = WebSocketsServer(81);

void handleAccelerometer()
{
    int16_t bias = 0;
    const int arr_size = 5000;
    char *html_code = new char[arr_size];
    memset(html_code, '\0', sizeof(char) * arr_size);

    bias += getHtml(HTML_BEGIN, *html_code, arr_size, 0);
    // bias += getHtml(STYLE, *html_code, arr_size, bias);
    // bias += getHtml(SCRIPT, *html_code, arr_size, bias);
    // bias += getHtml(HTML_END, *html_code, arr_size, bias);

    server.send(200, "text/html", html_code);

    delete[] html_code;
}

void cookAccelerometer(String &in, int *in_array, int array_size) {
    String *output = &in;
    int *array = in_array;
    DynamicJsonBuffer jsonBuffer;
    JsonObject& root = jsonBuffer.createObject();
    JsonArray& data = root.createNestedArray("data");
    for (int i = 0; i != array_size; i++) {
        data.add(*(array + i));
    }
    root.printTo(*output);
}

int lowFreqFilter(int inputValue, int lastFilteredValue, int alpha) {
    return (lastFilteredValue +  alpha * (inputValue - lastFilteredValue)+100);
}

void updateAccelerometer()
{
    if ((millis() - accelerometer.lastUpdate) > (1000 / graphAcc.updateRate))
    {   
        int filteredSignal = 0;
        int sourceSignal = analogRead(A0);
        accelerometer.lastValue = map(sourceSignal, 0, 1023, graphAcc.size-1, 0);

        if (graphAcc.counter != 0) {
            //filteredSignal = lowFreqFilter(accelerometer.lastValue, graphAcc.array[acc.graphCounter - 1], 0.5);
            filteredSignal = graphAcc.array[graphAcc.counter - 1] + 0.5 * (accelerometer.lastValue - graphAcc.array[graphAcc.counter - 1]);
        }

        if (graphAcc.counter < graphAcc.size - 1)
        {   
            graphAcc.array[graphAcc.counter] = filteredSignal;
            graphAcc.source[graphAcc.counter] = sourceSignal;
            graphAcc.counter++;
        }
        else
        {
            for (int i = 0; i != graphAcc.counter; i++)
            {
                graphAcc.array[i] = graphAcc.array[i + 1];
                graphAcc.source[i] = graphAcc.source[i + 1];
            }
            graphAcc.array[graphAcc.counter] = filteredSignal;
            graphAcc.source[graphAcc.counter] = sourceSignal;
            
            //start fft
            for (int i = 0; i < graphAcc.size; i++) {
                vReal[i] = (double) graphAcc.source[i];
                vImag[i] = 0;
            }
            FFT.Windowing(vReal, graphAcc.size, FFT_WIN_TYP_RECTANGLE, FFT_FORWARD);
            FFT.Compute(vReal, vImag, graphAcc.size, FFT_FORWARD);
            FFT.ComplexToMagnitude(vReal, vImag, graphAcc.size);
            for (int i = 0; i < graphAcc.size/2; i++) {
                graphAcc.fft[i] = map((int) vReal[i], 0, 15000, 200, 0);
            }
        }

        accelerometer.lastUpdate = millis();
    }
}



void handleNotFound()
{
    String message = "File Not Found\n\n";
    message += "URI: ";
    message += server.uri();
    message += "\nMethod: ";
    message += (server.method() == HTTP_GET) ? "GET" : "POST";
    message += "\nArguments: ";
    message += server.args();
    message += "\n";
    for (uint8_t i = 0; i < server.args(); i++)
        message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
    server.send(404, "text/plain", message);
}

void sendAccelerometerData() {

    int updateRate = 0;
    if (fftMode) updateRate = 200;
    else updateRate = graphAcc.updateRate;

    if (millis() - lastDataSend > updateRate) {
        String word;
        cookAccelerometer(word, graphAcc.fft, graphAcc.size/2);
        webSocket.broadcastTXT(word);
        Serial.println(word);
        lastDataSend = millis();
    }
}


void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length) {
    switch(type) {
        case WStype_TEXT:
            timeOfCal = (int) *payload;
            Serial.printf("[%u] get Text: %d\n", num, timeOfCal);
    }
}



void setup(void)
{
    IPAddress apIP(192, 168, 1, 1);
    Serial.begin(115200);
    Serial.setDebugOutput(true);
    Serial.println();
    
    WiFi.setAutoConnect(false); 
    WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
    WiFi.softAP(ssid);
    IPAddress myIP = WiFi.softAPIP();
    Serial.print("AP IP address: ");
    Serial.println(myIP);

    server.on("/accelerometer", handleAccelerometer);
    server.on("/", []() {
        server.send(200, "text/html", "<a style=\"font-size:10em;2\"href=\"http://192.168.1.1/accelerometer\">Main Page</a>");
    });
    server.onNotFound(handleNotFound);

    server.begin();
    Serial.println("HTTP server started");
    
    webSocket.begin();
    webSocket.onEvent(webSocketEvent);

    pinMode(A0, INPUT);
}

void loop(void)
{   
    updateCpuTime(0);

    server.handleClient();
    webSocket.loop();
    updateAccelerometer();
    sendAccelerometerData();
    
    updateCpuTime(1);
    printCpuLoad();
}